<?php
$request = $_SERVER['REQUEST_URI'];

$salt = "This!..yIswnozGoodqSaltp?!,idk";

$pswFile = '../psw.csv';
$sessFile =  '../sessions.csv';

if (!file_exists($pswFile)) {
    touch($pswFile);
}
if (!file_exists($sessFile )) {
    touch($sessFile );
}


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['registerName']) && isset($_POST['registerPassword'])) {

        $account = $_POST['registerName'];
        $password = $_POST['registerPassword'];

        $new_line = md5($account . $salt) . ',' . md5($password . $salt) . "\n";

        if (file_put_contents($pswFile, $new_line, FILE_APPEND | LOCK_EX)) {
            echo "true";
            return;
        }

        echo "false";
    } else if (isset($_POST['loginName']) && isset($_POST['loginPassword'])) {
        $account = $_POST['loginName'];
        $password = $_POST['loginPassword'];

        $needle = md5($account . $salt) . ',' . md5($password . $salt) . "\n";

        if (strpos(file_get_contents($pswFile), $needle) !== false) {
            $randomHash = md5(bin2hex(rand(0,999999999)));
            $new_line = $randomHash . ',' . date("d.m.Y") . "\n";

            file_put_contents($sessFile , $new_line, FILE_APPEND | LOCK_EX);

            echo $randomHash;
            return;
        }

        echo "unregistered";
    } else if (isset($_POST['authSession'])) {
        $needle = $_POST['authSession'];

        if (strpos(file_get_contents($sessFile), $needle) !== false) {
            echo "true";
            return;
        }

        echo "false";
    } else if (isset($_POST['newTopicName']) && isset($_POST['newTopicSubName'])) {
        $newTopicName = $_POST['newTopicName'];
        $newTopicSubName = $_POST['newTopicSubName'];
        $newTopicContent = $_POST['newTopicContent'];
        $newTopicSources = $_POST['newTopicSources'];

        $jsonTopics = file_get_contents('wwwContent.json');
        $data = json_decode($jsonTopics, true);

        if ($data[$newTopicName] !== null) {
            $data[$newTopicName][$newTopicSubName] = [];
            $data[$newTopicName][$newTopicSubName]["content"] = $newTopicContent;
            $data[$newTopicName][$newTopicSubName]["references"] = $newTopicSources;
        } else {
            $data[$newTopicName] = [];
            $data[$newTopicName][$newTopicSubName] = [];
            $data[$newTopicName][$newTopicSubName]["content"] = $newTopicContent;
            $data[$newTopicName][$newTopicSubName]["references"] = $newTopicSources;
        }

        $newJsonTopics = json_encode($data);
        file_put_contents('wwwContent.json', $newJsonTopics);

        echo "Successfully added Topic.";

    } else {
        print_r($_POST);
        echo "Some Error occured.";
    }
} else if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // HTTP
}

