import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

// Autor: Robin Steinwarz

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/aboutMe',
    name: 'Steckbrief',
    component: () => import( '../views/AboutMe.vue')
  },
  {
    path: '/colorGame',
    name: 'Farbspiel',
    component: () => import( '../views/ColorGame.vue')
  },
  {
    path: '/SolutionNavigator',
    name: 'SolutionNavigator',
    component: () => import( '../views/SolutionNavigator.vue')
  },

]

const router = new VueRouter({
  routes
})

export default router
