import Vue from 'vue'
import App from './App.vue'
import router from './router/router'
import store from './store/state'

Vue.config.productionTip = false
Vue.prototype.$hostname = "http://www2.inf.h-bonn-rhein-sieg.de/~rstein2s/backend/"
//Vue.prototype.$hostname = "http://localhost/www/WebEngBackend/"

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

