import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

// Autor: Robin Steinwarz

export default new Vuex.Store({
  state: {
    webPath: "/"
  },
  mutations: {

  },
  actions: {
  },
  modules: {
  }
})
